<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Imparte;

class TestController extends Controller
{
   public function testQuery()
   {
      //return Profesor::find(17601)->clases;
      //return Imparte::with(['profesor', 'clase', 'aula'])->where('id', 1)->get();
      //return Profesor::find('14281')->aulas;
      return Imparte::with(['profesor', 'clase', 'aula'])->where('id', 1)->get();
   }
}
