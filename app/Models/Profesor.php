<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profesor extends Model
{
   protected $table = "profesor";
   use HasFactory;

   public function clases() {
      return $this->belongsToMany(Clase::class,'imparte', 'p_idprofesor', 'c_codclase', 'id', 'codclase');
   }

   public function aulas() {
      return $this->belongsToMany(Aula::class,'imparte', 'p_idprofesor', 'a_idaula', 'id', 'id');
   }
}
