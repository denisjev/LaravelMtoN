<?php

namespace Database\Factories;

use App\Models\Profesor;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProfesorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Profesor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => $this->faker->unique()->numberBetween(10000, 90000),
            'nombre' => $this->faker->firstName(),
            'apellido' => $this->faker->lastName(),
            'titulo' => $this->faker->randomElement(["LIC","ING","MSC","DOC"])
        ];
    }
}
