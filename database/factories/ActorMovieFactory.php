<?php

namespace Database\Factories;

use App\Models\ActorMovie;
use App\Models\Movie;
use App\Models\Actor;
use Illuminate\Database\Eloquent\Factories\Factory;

class ActorMovieFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ActorMovie::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'movie_id' => Actor::inRandomOrder()->first()->id,
            'actor_id' => Actor::inRandomOrder()->first()->id
        ];
    }
}
