<?php

namespace Database\Factories;

use App\Models\Clase;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClaseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Clase::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
         'codclase' => $this->faker->unique()->numberBetween(10000, 90000),
         'nombre' => $this->faker->catchPhrase(),
         'credito' => $this->faker->randomDigit()
        ];
    }
}
