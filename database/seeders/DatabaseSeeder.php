<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         \App\Models\Actor::factory(10)->create();
         \App\Models\Movie::factory(10)->create();
         \App\Models\ActorMovie::factory(40)->create();
         \App\Models\Profesor::factory(20)->create();
         \App\Models\Clase::factory(20)->create();
         \App\Models\Aula::factory(20)->create();
         \App\Models\Imparte::factory(60)->create();
         \App\Models\Estudiante::factory(20)->create();
    }
}
